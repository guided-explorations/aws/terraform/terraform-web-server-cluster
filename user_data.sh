#!/bin/bash
if ! systemctl is-active --quiet amazon-ssm-agent ;then
  echo "SSM not found, installing for troubleshooting..."
  if [[ "$(uname -i)" == "aarch64" ]];then PKGARCH=arm64; else PKGARCH=amd64; fi
  sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_$PKGARCH/amazon-ssm-agent.rpm 
  sudo systemctl restart amazon-ssm-agent
  sudo systemctl status amazon-ssm-agent
fi
echo "Installing httpd..."
sudo dnf install -y httpd
sudo systemctl start httpd
sudo systemctl enable httpd
sudo systemctl is-enabled httpd
sudo mkdir -p /var/www/html
sudo sed -i '/Listen [0-9]*/s//Listen 8080/' /etc/httpd/conf/httpd.conf
sudo systemctl restart httpd
TOKEN=$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")
MYINSTANCEID=$(curl -q -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/instance-id)
echo "Configuring web page"
sudo tee /var/www/html/index.html > /dev/null <<HTMLHomePage
<html><head><title>GitLab Terraform Example Application</title></head>
<body bgcolor="white">
<A HREF="https://about.gitlab.com"><IMG SRC="https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-rgb.png" width="200"></A>
<h1>GitLab Terraform Example Application</h1>
<h2>Environment: ${GL_ENV}</h2>
<p>Some details about this instance.
<BR>Page retrieved from load balanced instance: $MYINSTANCEID
<BR>Do a browser refresh to see the instance id change to other instances in the ASG.</p>
</body>
</html>
HTMLHomePage