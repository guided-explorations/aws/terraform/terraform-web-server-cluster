# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CREATE ALL THE RESOURCES TO DEPLOY AN APP IN AN AUTO SCALING GROUP WITH AN ELB
# This template runs a simple "Hello, World" web server in Auto Scaling Group (ASG) with an Elastic Load Balancer
# (ELB) in front of it to distribute traffic across the EC2 Instances in the ASG.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


terraform {
  # This module is now only being tested with Terraform 1.1.x. However, to make upgrading easier, we are setting 1.0.0 as the minimum version.
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 5.14"
    }
  }
}

# ------------------------------------------------------------------------------
# CONFIGURE OUR AWS CONNECTION
# ------------------------------------------------------------------------------
# Use CI Variables for AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION

provider "aws" {
}

# ---------------------------------------------------------------------------------------------------------------------
# GET THE LIST OF AVAILABILITY ZONES IN THE CURRENT REGION
# Every AWS accout has slightly different availability zones in each region. For example, one account might have
# us-east-1a, us-east-1b, and us-east-1c, while another will have us-east-1a, us-east-1b, and us-east-1d. This resource
# queries AWS to fetch the list for the current account and region.
# ---------------------------------------------------------------------------------------------------------------------

resource "random_string" "unique4" {
  length  = 4
  special = false
  upper   = true
  keepers = {
    stack_unique_slug = var.stack_unique_slug
  }
}

data "aws_availability_zones" "all" {
  state = "available"
}

data "aws_ami" "latest_amazon_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["al2023-ami-minimal-*-kernel-6*-arm64"] #latest amazon linux 2023 non-minimal kernel and non-ecs
  }
  # ["al2023-ami-2023*-kernel-6*-arm64"] #latest amazon linux 2023 non-minimal kernel and non-ecs
  # ["al2023-ami-minimal-*-kernel-6*-arm64"] #latest amazon linux 2023 non-minimal kernel and non-ecs
  # ["amzn2-ami-hvm-*-arm64-gp2"] #latest amazon linux 2
  # to find other name queries, vary the `values` bit in this: 
  #    `aws ec2 describe-images --owners amazon --filters "Name=name,Values=al2023-ami-2023*-kernel-6*-arm64" --query 'sort_by(Images, &CreationDate)[].Name'`  
  owners = ["amazon"]
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A LAUNCH TEMPLATE FOR THE ASG
# ---------------------------------------------------------------------------------------------------------------------
# NOTE: To debug instance scripts either:
#       - review the system log in the EC2 console where this userdata script will generate log lines
#       - or manually attach an instance profile that gives SSM Session Manager permissions and use
#.        use the aws console to login with SSM Session Manager

locals {
  vars = {
    GL_ENV = var.stack_unique_slug
  }
}
resource "aws_launch_template" "example" {
  name_prefix            = var.stack_unique_slug
  image_id               = data.aws_ami.latest_amazon_linux.id
  instance_type          = "t4g.small"
  vpc_security_group_ids = [aws_security_group.instance.id]
  update_default_version = true
  user_data              = base64encode(templatefile("user_data.sh", { GL_ENV = var.stack_unique_slug }))
  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size           = 8
      delete_on_termination = true
    }
  }
  lifecycle {
    create_before_destroy = true
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE AUTO SCALING GROUP
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_autoscaling_group" "example" {
  name = "${substr(var.stack_unique_slug, 0, 23)}-${random_string.unique4.result}-asg" # not to exceed 32 ("23 + "-" + 4 character random_id + "-asg")
  #Enable Spot Support 
  mixed_instances_policy {
    instances_distribution {
      on_demand_percentage_above_base_capacity = 0
      spot_instance_pools                      = 2
    }
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.example.id
        version            = aws_launch_template.example.latest_version
      }
      override {
        instance_type = "t4g.nano"
      }
      override {
        instance_type = "t4g.micro"
      }
    }
  }
  lifecycle {
    ignore_changes        = [desired_capacity]
    create_before_destroy = true
  }
  ignore_failed_scaling_activities = true
  availability_zones               = data.aws_availability_zones.all.names
  min_size                         = 2
  max_size                         = 10
  load_balancers                   = [aws_elb.example.name]
  health_check_type                = "ELB"
  instance_refresh {
    strategy = "Rolling"
    preferences {
      #instance_warmup = 300 # Default behavior is to use the Auto Scaling Group's health check grace period.
      min_healthy_percentage = 50
    }
    triggers = ["launch_template", "desired_capacity"] # You can add any argument from ASG here, if those has changes, ASG Instance Refresh will trigger
  }
  tag {
    key                 = "Name"
    value               = "${var.stack_unique_slug}-terraform-asg-example"
    propagate_at_launch = true
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE THE SECURITY GROUP THAT'S APPLIED TO EACH EC2 INSTANCE IN THE ASG
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_security_group" "instance" {
  name = "${substr(var.stack_unique_slug, 0, 200)}-${random_string.unique4.result}-inst-sg" # not to exceed 255

}

resource "aws_security_group_rule" "ingress" {
  type              = "ingress"
  from_port         = var.server_port
  to_port           = var.server_port
  protocol          = "tcp"
  security_group_id = aws_security_group.instance.id
  cidr_blocks       = ["0.0.0.0/0"]
  #source_security_group_id = aws_security_group.elb.id
}

# Do not lock this to the elb as doing so disables many AWS hypervisor things like SSM, AWS yum repositories, console logs, etc.
resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.instance.id
  cidr_blocks       = ["0.0.0.0/0"]
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE AN ELB TO ROUTE TRAFFIC ACROSS THE AUTO SCALING GROUP
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_elb" "example" {
  name               = "${substr(var.stack_unique_slug, 0, 23)}-${random_string.unique4.result}-elb" # not to exceed 32 ("23 + "-" + 4 character random_id + "-elb")
  security_groups    = [aws_security_group.elb.id]
  availability_zones = data.aws_availability_zones.all.names

  health_check {
    target              = "HTTP:${var.server_port}/"
    interval            = 90
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 3
  }

  # This adds a listener for incoming HTTP requests.
  listener {
    lb_port           = var.elb_port
    lb_protocol       = "http"
    instance_port     = var.server_port
    instance_protocol = "http"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A SECURITY GROUP THAT CONTROLS WHAT TRAFFIC AN GO IN AND OUT OF THE ELB
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_security_group" "elb" {
  name = "${substr(var.stack_unique_slug, 0, 200)}-${random_string.unique4.result}-elb-sg" # not to exceed 255

  # Allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound HTTP from anywhere
  ingress {
    from_port   = var.elb_port
    to_port     = var.elb_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

