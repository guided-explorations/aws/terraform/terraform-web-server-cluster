
# GitLab's Terraform Templates perform transparent, integrated state management with 
# it's built-in feature: https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
# The following terrform code is required for GitLab state management to be used.
terraform {
  backend "http" {
  }
}