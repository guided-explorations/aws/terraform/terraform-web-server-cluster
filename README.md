# GitLab Terraform Example

[backendgitlab.tf](backendgitlab.tf) - this terraform file forces the terraform state backend to use http. This is required for the built-in capabilities of GitLab.

Why does every environment and cloud resource include the GitLab project ID? Read about it in the comments for variable `TF_VAR_stack_unique_slug` in [.gitlab-ci.yml](.gitlab-ci.yml) - you can disable it if you can guarantee stack uniqueness by another method - such as deployment of only one GitLab project per AWS account.

## Tutorials That Use This Working Code

**IMPORTANT:** Requirements for self-paced execution of these tutorials are covered in [TUTORIAL-Requirements.md](TUTORIAL-Requirements.md)

[TUTORIAL.md](TUTORIAL.md) - Covers the basics of deploying a serverless application to a private merge request isolated Managed DevOps Environment and performing Terraform SAST scanning on the code.

[TUTORIAL2-SecurityAndManagedEnvs.md](TUTORIAL2-SecurityAndManagedEnvs.md) - builds upon TUTORIAL.md to demonstrate Security Policy Merge Approval Rules and how they encourage a developer to resolve new vulnerabilities in the code changes they’ve made before being allowed to merge into a shared branch. It also shows how Managed DevOps Environments allow full preview of application changes in an Per-Merge Request isolated environment and how that environment is automatically destroyed when merging into the default branch to update the Production environment.

[TUTORIAL3-Environments.md](TUTORIAL3-Environments.md) - shows how the branch based environments that make MR environments possible also open up a world of possibilities for GitOps workflows through multiple pre-production environments (e.g. Feature => Integration => Stage => Production). Also shows how branches can function as self-service experimental environment provisioning and disposal - including for older versions of the stack.

## Visual Overview

### Tutorial 1
![terraformtutorial1overview](images/terraformtutorial1overview.png)

### Tutorial 2
![terraformtutorial2overview](images/terraformtutorial2overview.png)

### Tutorial 3

![terraformtutorial3gitlabenvironments](images/terraformtutorial3gitlabenvironments.png)

### Manual Cleanup

When experimenting you may get situations where your Terraform state is no longer tracking your stack or is otherwise unavailable, manual cleanup is simple with this example by following this sequence for the AWS resources created by the deployment:
1. Delete the ASG
2. Delete the load balancer
3. Wait for instances to be deleted by prior ASG deletion - delete manually if there are any problems.
4. Delete the security groups
5. In GitLab UI, delete the state in <yourproject> => operate => terraform states

### Design Objectives

Square bracketted categories indicate in which domains specific techniques and code had to be used to get the desired result.

1. Works out of the box with GitLab's built in Terraform templates [GitLab CI]
    - embedded terraform file ([backendgitlab.tf](backendgitlab.tf) for enabling http backend to ensure templates work to show GitLab's built-in state management as per [GitLab Terraform Docs](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#initialize-a-terraform-state-as-a-backend-by-using-gitlab-cicd)
2. Enable easy end to end CI pipeline demonstrations yet be somewhat real-world by being a web server cluster [GitLab CI + AWS]
    - Simple web server with page background color and other text that can be changed to observe end to end changes and different settings in different environments.
    - easy manual clean up due to few resources
3. Inexpensive stack for training and for leaving running as a working example - as of this writing cost is $0.22 USD/Day ($6.75/week) for two graviton micros on spot [AWS + Terraform]
    - Uses less expensive graviton ARM instances
    - Uses very small instances (micro)
    - Uses Amazon Linux 2023 minimal kernel AMI 
    - Sets them up as spot by default
4. Support simplified security demonstrations and tutorials [GitLab CI + Terraform]
    - code contains vulnerabilities that will be detected by Gitlab scanners
    - one of which can be user-resolved to demonstate the power of Security Policy Approval Rules
    - a future version will embed a security policy rule to simplify demos and tutorials for that scenario
5. Branch based GitLab DevOps Environment support with multiple environments per AWS region [GitLab CI + Terraform]
    - The terraform code is structured to create all unique resources in a deployment by taking a variable called `stack_unique_slug` combined with terraofrm random_string and some length limiting code. The .gitlab-ci.yml code is used to pass the value of stack_unique_slug as one of the predefined CI slug variables. This enables it to be deployed many times into the same environment.Especially important for global IAM resources. This approach is important since GitLab's templates cannot currently support Terraform Workspaces (another mechanism for unique namespacing Terraform created resources)
    - The rules for the existing template are overridden to enable deployment on MR/Feature branches
6. Terraform template properly propagates all changes made to the Terraform code to the stack for AWS ASG [AWS + Terraform]
    - required usage of instance_refresh to get this to happen for an ASG configuration.
7. Uses best practice AWS IaC principles
    - Amazon Linux 2023 [AWS + Terraform] - which requires IDSMv2 for instance metadata retrieval
    - Uses LaunchTemplates for ASG instance configuration
8. SSM ready for troubleshooting on-instance provisioning code [AWS + Terraform]
    - ensure SSM agent is installed even when using minimal AL 2023 kernel.
